const cron = require('node-cron');
const { parse } = require('rss-to-json')
const config = require('./config.json');
const Aria2 = require("aria2");
const fs = require("fs");
const random = require("./utils/random");
const chokidar = require("chokidar");
const { execSync } = require('child_process');
const replace = require("./utils/replace")

const aria2 = new Aria2({
  host: 'localhost',
  port: 6800,
  secure: false,
  secret: config.aria2Secret,
  path: '/jsonrpc'
});

let downloadQueue = [];

const tmpDir = config.tmpDir

cron.schedule(config.frequency, () => {
  for (let i = 0; i < config.shows.length; i++) {
    let feedUrl = config.shows[i].feedUrl;

    parse(feedUrl).then(async rss => {
      let episodes = rss.items.filter(item => item.title.includes(config.shows[i].matchShow) && item.title.includes(config.shows[i].matchRelease))
      if (episodes.length != 0) {
        if (!episodes[0].title.includes(`S${config.shows[i].currentSeason}E${config.shows[i].currentEpisode}`)) {
          const id = random(5);
          fs.mkdirSync(tmpDir + "/media-srvdl-" + id)
          await aria2.call("addUri", [episodes[0].link], { dir: tmpDir + "/media-srvdl-" + id });
          dlWatcher(id, episodes[0].title, config.shows[i].processing, config.shows[i].location, config.shows[i].rename, config.shows[i].preProcessingRemove)
          config.shows[i].currentEpisode = episodes[0].title.split(`S${config.shows[i].currentSeason}E`)[1].split(".")[0].replace("E", "")
          console.log("Started download of " + episodes[0].title)
        }
      }
    });
  }
})

function dlWatcher(id, title, processing, location, rename, preProcessingRemove) {
  chokidar.watch(tmpDir + '/media-srvdl-' + id).on('unlink', async (event, path) => {
    console.log("Finished download of " + title)
    let filePath = tmpDir + '/media-srvdl-' + id + "/" + title + "/" + title.replace(preProcessingRemove, "") + ".mkv"
    if (processing != "") {
      console.log("Starting processing")
      execSync(`ffmpeg -hwaccel nvdec -i "${filePath}" ${processing} "${tmpDir}/media-srvdl-${id}/transcode.mkv"`)
      filePath = `${tmpDir}/media-srvdl-${id}/transcode.mkv`
    }
    let newName = replace(title, preProcessingRemove, "")
    newName = replace(newName, ".", " ")
    newName = replace(newName, rename, "") + "mkv"
    fs.copyFileSync(filePath, location + `/${newName}`)
    console.log(title + " complete")
  });
}