# media-srv
Automatically download, rename, transcode, and organize your TV shows.

## Requirements
 - aria2

## Usage
Start aria2 with RPC.
```sh
aria2c --enable-rpc --rpc-listen-all=true --rpc-allow-origin-all
```
Then start media-srv.
```
node .
```

## Example config
```json
{
  "frequency": "0 * * * *", # Every hour
  "tmpDir": "/home/user/.cache/media-srv", # Create before running
  "aria2Secret": "", # Run aria2 with --rpc-secret
  "shows": [
    {
      "feedUrl": "https://example.com/rss",
      "matchShow": "My.TV.Show", # Releases with the episode name are not supported
      "matchRelease": "720p.WEB.x265-ExAmPLE[example]",
      "currentEpisode": "0", # Set if you already have part of the season downloaded
      "rename": " 720p WEB x265-ExAmPLE[example]", # Dots are automatically converted to spaces
      "processing": "-c:v hevc_nvenc -c:a aac", # Leave blank to disable
      "location": "/home/user/Videos/My TV Show/Season 5"
    }
  ]
}
```